<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index () {
        $users = User::all();
        $user = new User;
        return view('user.index', compact('users', 'user'));
    }

    public function create () {
        $user = new User;
        return view('user.create', compact('user'));
    }

    public function store (Request $request) { 
        $user = User::create($request->all());

        $user->status = $user->status == 1 ? "Active" : "Inactive";

        return response()->json([
            'user' => $user
        ]);
    }

    public function edit ($id) {
        $user = User::find($id);

        return view('user.edit', compact('user'));
    }

    public function update ($id, Request $request) {
        $user = User::find($id);
        $user->update($request->all());
        $user->status = $user->status == 1 ? "Active" : "Inactive";
        return response()->json([
            'user' => $user
        ]);
    }

    public function delete($id) {
        $user = User::find($id);
        $user->delete();

        return response()->json([
            'id' => $id
        ]);
    }
}
