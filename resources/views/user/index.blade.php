<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
                margin-top: 1%;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .form-group {
                margin-bottom: 8px;
                margin-top: 1px;
            }
        </style>
    </head>
    <body>
        <div class="container-lg">
            <div id="div-table" class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-lg-10">
                                    <h4>Users</h4>
                                </div>
                                <div class="col-lg-2" style="text-align: right">
                                    <button id="addUser" class="btn btn-sm btn-primary">
                                        <i style="color: white" class="fas fa-user-plus"></i> Add User
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <table id="table-user" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>
                                            Name
                                        </th>
                                        <th>
                                            Lastname
                                        </th>
                                        <th>
                                            Email
                                        </th>
                                        <th>
                                            Status
                                        </th>
                                        <th style="text-align: center">
                                            ...
                                        </th>
                                    </tr>
                                </thead>
                                <tbody id="tbody-user">
                                    @foreach ($users as $item)
                                        <tr id="item-{{ $item->id }}">
                                            <td id="name-{{ $item->id }}">{{ $item->name }}</td>
                                            <td id="lastname{{ $item->id }}">{{ $item->lastname }}</td>
                                            <td id="email-{{ $item->id }}">{{ $item->email }}</td>
                                            <td id="status-{{ $item->id }}">{{ $item->status == 1 ? 'Active' : 'Inactive' }}</td>
                                            <td style="text-align: center">
                                                <button data-id="{{ $item->id }}" class="btn btn-sm btn-info btnEdit"><i style="color: white" class="fas fa-user-edit"></i></button>
                                                <button data-id="{{ $item->id }}" class="btn btn-sm btn-danger btnDelete"><i class="fas fa-user-slash"></i></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div id="div-form" class="row">
                
            </div>
        </div>
    </body>
</html>

<script src="{{ asset('plugins/lib/jquery-1.11.1.js') }}"></script>
<script src="{{ asset('plugins/dist/jquery.validate.min.js') }}"></script>
<script src="{{ asset('plugins/dist/additional-methods.min.js') }}"></script>
<script src="{{ asset('plugins/dist/localization/messages_es.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js" integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/877942ed8c.js" crossorigin="anonymous"></script>

<script>
    $(function() {
        $("#addUser").click(function() {
            $.ajax({
                type: "get",
                url: '{{ route("user.add") }}',
                contentType: false,
                processData: false,
                dataType: 'html',
                success: function(response) {
                    $("#div-table").slideUp();
                    $("#div-form").html(response);
                    $("#div-form").slideDown();
                }
            });
        });
    });


    $(document).on('click', '.btnDelete', function () {
        if (window.confirm("Seguro quieres eliminar el usuario?")) {
            let id = $(this).data('id');
            let url = '{{ route("user.edit", ":slug") }}';
            url = url.replace(':slug', id);
            $.ajax({
                type: "delete",
                url: url,
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function(response) {
                    $("#item-"+response.id).remove();
                }
            });
        }
    });

    $(document).on('click', '.btnEdit', function () {
        let id = $(this).data('id');
        let url = '{{ route("user.edit", ":slug") }}';
        url = url.replace(':slug', id);
        $.ajax({
            type: "get",
            url: url,
            contentType: false,
            processData: false,
            dataType: 'html',
            success: function(response) {
                $("#div-table").slideUp();
                $("#div-form").html(response);
                $("#div-form").slideDown();
            }
        });
    });
</script>