<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <h4> Create user</h4>
        </div>
        <div class="card-body">
            {!! Form::open(['id' => 'fmrUser', 'route' => 'user.store']) !!}
                <meta name="csrf-token" content="{{ csrf_token() }}">
                {!! Form::hidden('id', $user->id, ['id' => 'id']) !!}
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            {!! Form::label('name', 'Name') !!}
                            {!! Form::text('name', $user->name, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            {!! Form::label('lastname', 'Lastname') !!}
                            {!! Form::text('lastname', $user->lastname, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            {!! Form::label('email', 'Email') !!}
                            {!! Form::email('email', $user->email, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            {!! Form::label('status', 'Status') !!}
                            <select name="status" class="form-control" id="status">
                                <option value="">Seleccione</option>
                                <option {{ $user->status == 1 ? "selected" : "" }} value="1">Active</option>
                                <option {{ $user->status == 2 ? "selected" : "" }} value="2">Inactive</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-12" style="text-align: right;">
                        <div class="form-group">
                            <button type="button" class="btn btn-sm" style="background: #f7f7f7">Close</button>
                            <button type="submit" id="btnGuardar" class="btn btn-sm btn-success">Save</button>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<script>
    $(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("#fmrUser").validate({
            submitHandler: function (form) {
                $.ajax({
                    url: '{{ route("user.update", $user->id) }}',
                    type: "POST",
                    data: $("#fmrUser").serialize(),
                    dataType: 'json',
                    success: function(response) {
                        let user = response.user;
                        console.log(user);
                        $("#name-"+user.id).text(user.name);
                        $("#lastname-"+user.id).text(user.lastname);
                        $("#email-"+user.id).text(user.email);
                        $("#status-"+user.id).text(user.status);
                        $("#div-table").slideDown();
                        $("#div-form").slideUp();
                    }
                });
            },
            rules: {
                name: { required: true },
                lastname: { required: true },
                email: { required: true },
                password: { required: true },
                status: { required: true },
            },
        });
    });
</script>