<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <h4> Create user</h4>
        </div>
        <div class="card-body">
            {!! Form::open(['id' => 'fmrUser', 'route' => 'user.store']) !!}
                <meta name="csrf-token" content="{{ csrf_token() }}">
                {!! Form::hidden('id', $user->id, ['id' => 'id']) !!}
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            {!! Form::label('name', 'Name') !!}
                            {!! Form::text('name', $user->name, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            {!! Form::label('lastname', 'Lastname') !!}
                            {!! Form::text('lastname', $user->lastname, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            {!! Form::label('email', 'Email') !!}
                            {!! Form::email('email', $user->email, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            {!! Form::label('password', 'Password') !!}
                            {!! Form::password('password', ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            {!! Form::label('status', 'Status') !!}
                            <select name="status" class="form-control" id="status">
                                <option value="">Seleccione</option>
                                <option {{ $user->status == 1 ? "selected" : "" }} value="1">Active</option>
                                <option {{ $user->status == 2 ? "selected" : "" }} value="2">Inactive</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-12" style="text-align: right;">
                        <div class="form-group">
                            <button type="button" class="btn btn-sm" style="background: #f7f7f7">Close</button>
                            <button type="submit" id="btnGuardar" class="btn btn-sm btn-success">Save</button>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<script>
    $(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("#fmrUser").validate({
            submitHandler: function (form) {
                $.ajax({
                    url: '{{ route("user.store") }}',
                    type: "POST",
                    data: $("#fmrUser").serialize(),
                    dataType: 'json',
                    success: function(response) {
                        let user = response.user;
                        console.log(user);
                        $("#tbody-user").append(`<tr id="item-`+ user.id +`">
                                <td id="name-`+ user.id +`">`+ user.name +`</td>
                                <td id="lastname-`+ user.id +`">`+ user.lastname +`</td>
                                <td id="email-`+ user.id +`">`+ user.email +`</td>
                                <td id="status-`+ user.id +`">`+ user.status +`</td>
                                <td style="text-align: center">
                                    <button data-id="`+ user.id +`" class="btn btn-sm btn-info btnEdit"><i style="color: white" class="fas fa-user-edit"></i></button>
                                    <button data-id="`+ user.id +`" class="btn btn-sm btn-danger btnDelete"><i class="fas fa-user-slash"></i></button>
                                </td>
                            </tr>`);
                        $("#div-table").slideDown();
                        $("#div-form").slideUp();
                    }
                });
            },
            rules: {
                name: { required: true },
                lastname: { required: true },
                email: { required: true },
                password: { required: true },
                status: { required: true },
            },
        });
    });
</script>