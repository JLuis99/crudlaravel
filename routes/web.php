<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('user', 'UserController@store')->name('user.store');
Route::post('user/{id}', 'UserController@update')->name('user.update');
Route::get('user/{id}', 'UserController@edit')->name('user.edit');
Route::get('user', 'UserController@index')->name('user.index');
Route::get('create-user', 'UserController@create')->name('user.add');
Route::delete('user/{id}', 'UserController@delete')->name('user.delete');